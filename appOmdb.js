/*----------------------------------------Requête Fetch API----------------------------------------*/

let movieNameRef = document.querySelector('#movie-name');
let searchBtn = document.querySelector('#search-btn');
let result = document.querySelector('#result');

key ="85e4900a";

let getMovie = () => {
    
    let movieName = movieNameRef.value;
    let url = `http://www.omdbapi.com/?t=${movieName}&apikey=${key}`;
    if(movieName.length <= 0){
        result.innerHTML = `
        <h3 class="msg">Veuillez saisir le nom d'un Film !</h3>
      `
    }else{
        fetch(url)
        .then((r) => r.json())
        .then ((data) => {

            if (data.Response == "True"){
                result.innerHTML  = `
                <div class="info">
                    <img src=${data.Poster} class="poster">
    
                    <div>
                        <h2>${data.Title}</h2>
    
                        <div class="details">
                            <span class="year">${data.Year}</span>
                        </div>
                            
                        <div class="director">
                            <span>${data.Director}</span>
                        </div>  
                    </div>
                </div>
                `
            }else{
                result.innerHTML =`<h3 class="msg">${data.erreur()}</h3>`
            }
            console.log(data);
            console.log(data.Poster)
            console.log(data.Title)
            console.log(data.Director)
            console.log(data.Year)
        })
        .catch(() => {
            result.innerHTML = `
            <h3 class="msg">Erreur</h3>
            `
        })
    }
};

searchBtn.addEventListener("click", getMovie);
window.addEventListener("load", getMovie);

