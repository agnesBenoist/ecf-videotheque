/*-----------------------------------Données tableau Javascript-----------------------------------------------*/

let films = [
    {
        title: "Deadpool",
        years: 2016,
        authors: "Tim Miller"
    },
    {
        title: "Spiderman",
        years: 2002,
        authors: "Sam Raimi"
    },
    {
        title: "Scream",
        years: 1996,
        authors: "Wes Craven"
    },
    {
        title: "It: chapter 1",
        years: 2019,
        authors: "Andy Muschietti"
    }
];


let rIndex, 
    table = document.querySelector('#table');

    function checkInput(){

        let estVide = false,
            titre = document.querySelector('#titre').value,
            date = document.querySelector('#date').value,
            auteur= document.querySelector('#auteur').value;

            if(titre === ""){
                alert ("titre est vide, veuillez saisir le champ !");
                estVide = true;
            }else if(date === ""){
                alert ("date est vide, veuillez saisir le champ !");
                estVide = true;
            }else if(auteur === ""){
                alert ("auteur est vide, veuillez saisir le champ !");
                estVide = true;
            }

        return estVide 
    }

    function addTableRow(){
        if(!checkInput()){
        let newRow = table.insertRow(table.length),
            cell1 = newRow.insertCell(0),
            cell2 = newRow.insertCell(1),
            cell3 = newRow.insertCell(2),
            titre = document.querySelector('#titre').value,
            date = document.querySelector('#date').value,
            auteur= document.querySelector('#auteur').value;
    
            cell1.innerHTML = titre;
            cell2.innerHTML = date;
            cell3.innerHTML = auteur;
    
            selectRow();
        }
    }

function selectRow(){
    
    
    for (let i = 1; i < table.rows.length; i++){
        table.rows[i].onclick = function(){
            rIndex = this.rowIndex;
            document.querySelector('#titre').value = this.cells[0].innerHTML;
            document.querySelector('#date').value = this.cells[1].innerHTML;
            document.querySelector('#auteur').value = this.cells[2].innerHTML;
            //console.log(rIndex);
        }
    }
}
selectRow();

function editTable(){
    
    let titre = document.querySelector('#titre').value,
    date = document.querySelector('#date').value,
    auteur = document.querySelector('#auteur').value;
    
  if(!checkInput()){
        table.rows[rIndex].cells[0].innerHTML = titre;
        table.rows[rIndex].cells[1].innerHTML = date;
        table.rows[rIndex].cells[2].innerHTML = auteur;
    }
    
}

function removeRow(){
    table.deleteRow(rIndex);
}



      
